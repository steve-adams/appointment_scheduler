# Problem
Create a class that checks to see if an appointment can be booked. It will check to see if a date/time (including the duration of the appointment) is being booked within its operation hours. Assume that everything is in the same time zone.

Create a class that accepts two inputs:

- appointment time/date
- the time length/duration of that appointment

Only allow appointments that are being booked from Monday to Friday, with appointment times starting from 7am and ending no later than 5pm. Return true if all the appointment falls within the limits, return false if not.

If you have time (not required): appointments should not be allowed to book over lunch time from 12-12:30 daily

## Solution

I used quite a few ActiveSupport features for the core logic. It allows easy range comparisons like 'are these hours inside of those hours?' and idiomatic checks for things like 'is this date a weekend?'. Another handy feature is the ability to concisely write a duration like `60.minutes` and add it to another time.

Since ActiveSupport is bundled with rails, it didn't seem like overkill to include it here. I'm assuming this would be for use in a Rails app.

I've also tried to design this so it's extensible and configurable in the future. The class constants should be straightforward to rewire as class properties for example. It would also be possible to add more conditions without much effort.

## Tests

To run the tests, clone this repo and run `ruby appointment_test.rb` in the root directory. The output should look like this:

```
$ ruby appointment_test.rb 
Started with run options --seed 8521

Appointment
  test_0002_books appointments on work days                       PASS (0.00s)
  test_0003_books appointments during work hours                  PASS (0.00s)
  test_0006_does not book appointments during lunch hours         PASS (0.00s)
  test_0001_has to be initialized with valid arguments            PASS (0.00s)
  test_0004_does not book appointments on weekends                PASS (0.00s)
  test_0005_does not book appointments outside of work hours      PASS (0.00s)

Finished in 0.00307s
6 tests, 8 assertions, 0 failures, 0 errors, 0 skips
```

The tests I wrote should cover all the expected behaviours described in the problem. I don't like the format of the test, but I'm not sure how to break up tests into smaller blocks yet. It also didn't seem necessary to figure out at the moment. I'd definitely prefer to organize tests a little better though.

If you see deprecation warnings, they're from ActiveSupport. I'm not sure which version of Ruby is required, but I used `2.6.2` with [rbenv](https://github.com/rbenv/rbenv).

## Performance

My goal was to create something easy to understand and test. I didn't put a lot of thought into performance yet because Ruby dates are a bit of a black box to me. Doing a deep dive into that would likely take up more than the two hours.