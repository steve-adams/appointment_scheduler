require 'date'
require 'active_support/all'

# Validates an appointment time and duration.
class Appointment

  WORK_HOURS = ('07:00'..'15:00').freeze
  LUNCH_HOURS = ('12:00'..'12:30').freeze

  # @param start    DateTime  The start time of the appointment
  # @param duration Integer   The duration of the appointment in minutes
  def initialize(start:, duration:)
    check_arguments(start: start, duration: duration)

    @start = start
    @time_range = start.strftime('%H:%M')..(start + duration).strftime('%H:%M')
  end

  # Checks if the appointment time is during work or lunch hours
  def valid
    on_work_day && inside_work_hours && outside_lunch_hours
  end

  # Check if self.start is on weekday
  def on_work_day
    @start.on_weekday?
  end

  # Check if start..finish is inside of WORK_HOURS
  def inside_work_hours
    WORK_HOURS.cover?(@time_range)
  end

  # Check if self.start..start.finish is overlaps LUNCH_HOURS
  def outside_lunch_hours
    !LUNCH_HOURS.overlaps?(@time_range)
  end

  # TODO: find a nice way to reduce line lengths
  def check_arguments(start:, duration:)
    raise ArgumentError, 'start must be DateTime' unless start.is_a?(DateTime)
    raise ArgumentError, 'duration must be ActiveSupport::Duration' unless duration.is_a?(ActiveSupport::Duration)
  end
end