require './appointment'
require 'minitest/spec'
require 'minitest/autorun'
require 'minitest/reporters'

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

# Test that appointments are correctly valid or invalid.
# @TODO: Make smaller test blocks.
class AppointmentTest < Minitest::Test

  describe Appointment do
    it 'has to be initialized with valid arguments' do
      # Invalid start
      assert_raises ArgumentError do
        Appointment.new(start: 'nope', duration: 60.minutes)
      end

      # Invalid duration
      assert_raises ArgumentError do
        Appointment.new(
          start: DateTime.parse('monday 7:30am'),
          duration: '60.minutes'
        )
      end
    end

    it 'books appointments on work days' do
      appointment = Appointment.new(
        start: DateTime.parse('tuesday 8am'),
        duration: 90.minutes
      )

      assert appointment.valid
    end

    it 'books appointments during work hours' do
      appointment = Appointment.new(
        start: DateTime.parse('monday 7am'),
        duration: 10.minutes
      )

      assert appointment.valid
    end

    it 'does not book appointments on weekends' do
      appointment = Appointment.new(
        start: DateTime.parse('saturday 7:30am'),
        duration: 60.minutes
      )

      refute appointment.valid
    end

    it 'does not book appointments outside of work hours' do
      # Starts before work hours
      early_appointment = Appointment.new(
        start: DateTime.parse('monday 6am'),
        duration: 30.minutes
      )

      # Starts during work hours but ends after work hours
      late_appointment = Appointment.new(
        start: DateTime.parse('monday 4pm'),
        duration: 90.minutes
      )

      refute early_appointment.valid
      refute late_appointment.valid
    end

    it 'does not book appointments during lunch hours' do
      appointment = Appointment.new(
        start: DateTime.parse('monday 11am'),
        duration: 90.minutes
      )

      refute appointment.valid
    end
  end
end